# What is this
It's a sample application written using the GNU Artanis web framework.


I'll be writing more documentation about how I created this in the short future.
For the time being you can refer to the official GNU Artanis documentation as a guideline.  
However, lots of things are wrong in the documentation right now, so this code should serve as a working reference.   

This application uses a mysql database to search for data. And a local dir path to check for saved data. It's an sample CD searching interface, a rebuild of an old php interface I used to have a long time ago, when HDDs were too expensive for me.

# Prerequisites
1. guile
2. guile-dbi
3. artanis
4. super minimal mysql experience (can create databases and add data)

You can check my guile and artanis ansible roles, also in gitlab, to do a similar installation to mine if these requirements sound challenging.

# To do before starting artanis
## add sample data
This project uses the `pub/cd_contents` dir to save directories that must match the `cd_id` field in the database.  
Like:  
```
.
./e001
./e001/files.txt
./e001/some_pic.jpg
./e001/some_other_pic.jpg
```
Where:  
1. files.txt is a list of files actually in the disk (not really used... but in my old php interface I used to grep these contents, maybe I'll add that feature someday).
1. *jpg are image files showing the contents of the disk, like a cover pic of a photo album/movie, etc (not all the actual files in the disk, but I guess you could use it for that too).


## mysql stuff
1. do data migration 
  `art migrate up description`
2. add some sample data. The cd_id must match a dir name in the `pub/cd_contents` path.  
  I have a "cds2" database, with a "description" table in my mysql server. It looks like this:
```
MariaDB [cds2]> describe description
    -> ;
+----------+---------------------+------+-----+---------+----------------+
| Field    | Type                | Null | Key | Default | Extra          |
+----------+---------------------+------+-----+---------+----------------+
| id       | bigint(20) unsigned | NO   | PRI | NULL    | auto_increment |
| cd_id    | varchar(4)          | YES  |     | NULL    |                |
| category | varchar(32)         | YES  |     | NULL    |                |
| contents | varchar(1024)       | YES  |     | NULL    |                |
| artists  | varchar(256)        | YES  |     | NULL    |                |
+----------+---------------------+------+-----+---------+----------------+
5 rows in set (0.01 sec)
```

You can add data with something like:
```
insert into description(cd_id, category, contents, artists) values('e001', 'entertainment', 'of the adult type', 'Hot blonde Girl1');
```
3. Change the user/password for the mysql connection account in the code. In this code the user is `guileuser`, and the password is `somepassword`, grep for these strings and replace them as fit.

# Steps to build from scratch (quick step by step guide for new projects)
1. build project
`art create cds`

2. create controller and view
`art draw controller cd show`

3. create database migration for initial mysql data formatting
after changing the database user/password in the conf file
```
# creates the migration data
art draw migration description

# actually runs the mysql queries to create the table
art migrate up description
```

4. copied all data from my old DB/disk (as described above)
5. ran art work in debug mode, then production mode
```
# test with verbose output
art work --debug

# after tests are done
art work
```

# Challenges I had
1. Mostly the documentation.  
It is a mess. I sent a PR to correct some of the English grammar (I'm guessing the authors were mostly chinese...), but apparently new docs are only updated on major version releases, so it might take a while to actually see the doc on the site.
2. Japanese support.  
I had to install the locales-all debian package so guile would be able to understand/show Japanese. And call the mysql query `set names utf8;` before doing any queries inside artanis now for artanis to read it. Yeah, not something that's usually in the documentation.
3. No working "model". So this shouldn't really be called an MVC model framework. The database/data model doesn't exist in working mode, but just in lots of testing code that mostly doesn't work.
4. variables/functions have to mostly be searched in the code directly (again, lack of documentation I guess).

# Overall thoughts
1. guile is great. It's fun to read/write. Especially if you are using emacs and are used to emacs lisp. Artanis itself is a great idea.
2. artanis has a long way to go... development appears to be super slow. I'm guessing too few people writing it. And the testing quality is very low at best (code that used to work in one version stops working in the next without warning. Even stuff that could easily be tested automatically if test code existed).
3. The documentation needs a lot of improvement. Right now it's in org format (which is exported to html I guess), not the most popular format for documentation out there, but yes, popular for emacs users. I wonder if all artanis users also use emacs.
4. There are language barries with the developer(s). You have a question in English, and you get an answer, but mostly for another question or sometimes just "comments" instead of an answer to your specific question.
5. Not having a model to handle database tables as data is actually OK. But it's not cool to call this an MVC model framework when it has no model. Maybe VC framework is more appropiate, at least on this stage.
6. . It has other limitations, like not being to show file names with spaces for static files (it doesn't translate/decode back from URL encoding to plain text due to security issues I guess?). And POST data in Japanese isn't parsed either... very annoying.

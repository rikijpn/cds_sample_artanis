;; Controller cds definition of cds
;; Please add your license header here.
;; This file is generated automatically by GNU Artanis.
(define-artanis-controller cds) ; DO NOT REMOVE THIS LINE!!!

;; modules used by the controller, to declare per file/controller
(use-modules (ice-9 ftw)
	     (ice-9 regex)
	     (srfi srfi-1)
	     )


;; you can declare all functions/variables used by this controller at the top
;; so they're in the scope with all following functions

;; database info
(define init-str "guileuser:somepassword:cds2:tcp:localhost:3306")

;; global functions

(define get-cd-id
  ;; get the cd id from the query string
  (lambda (rc)
    (let ((cd-id (get-from-qstr rc "cd_id")))
      cd-id)))

(define get-all-pics
  ;; gets all pics in the dir for this cd and returns them in a list
  (lambda (cd-id)
    (let* (
	   (cd-id (string-downcase cd-id))
	   (dirname (string-append (current-toplevel) "/pub/cd_contents/" cd-id))
	   ;;current-toplevel = your project dir path
	   (files (scandir dirname))
	   (files-list #f))
      (and files
	   (not (eq? '()
		     ;; overwrite dummy files-list with all the
		     ;; files having a pic file extension
		     (set! files-list
		       (append
			(filter-map (lambda (item) (string-match "^.*.jpg" item)) files)
			(filter-map (lambda (item) (string-match "^.*.JPG" item)) files)
			(filter-map (lambda (item) (string-match "^.*.png" item)) files)
			))))
	   files-list
	   ))))

(define show-all-pics
  ;; generates the html to actually put the found pics in the page
  (lambda (rc)
    (let* (
	   (cd-id (get-cd-id rc))
	   (pics (get-all-pics cd-id))
	   (base-dir (string-append "/pub/cd_contents/" cd-id "/")))
      (if (and pics (not (eq? pics '())))
	  (tpl->html
	   (append
	    (map
	     (lambda (pic)
	       (list 'img `(@
			    (src ,(string-append base-dir (vector-ref pic 0)))))) pics)))
	  ;; else
	  "no pics here"))))

(define make-link-for-pics-list
  ;; returns a link (<a href=XX>some string</a>) if the cd-id dir has pics
  (lambda (cd-id)
    (let* (
	   (cd-id (string-downcase cd-id))
	   (dirname (string-append (current-toplevel) "/pub/cd_contents/" cd-id))
	   (files (scandir dirname))
	   (link-path (string-append "show_pics?cd_id=" cd-id )))
      (if 
       ;; add link to files only if there are any pic files in the dir for that cd
       ;; otherwise, return N/A
       (and files
	    (not (eq? '()
		      (filter-map ;; like map, but returns only true values
		       (lambda (item) (string-match "^.*.jpg" item)) files))))
       `(a (@ (href ,link-path)) "pics available")
       "N/A"))))

(define make-link-for-files-list
  ;; returns a link if the cd dir has a "files.txt" file
  (lambda (cd-id)
    (let* ((main-contents-dir (current-toplevel))
	   (relative-path "/pub/cd_contents/")
	   (cd-id (string-downcase cd-id))
	   (file-name (string-append main-contents-dir
				     relative-path
				     cd-id "/files.txt"))
	   (link-path (string-append relative-path cd-id "/files.txt")))
      (if
       (file-exists? file-name)
       `(a (@ (href ,link-path)) "files list")
       "N/A"))))

(define print-row 
  ;; prints a (single) row for the info table showing the DB data
  ;; called in the view file
  (lambda (row)
    (let ((cd-id (cdr (cadr row))))
      (tpl->html
       (list 'tr
	     (append
	      ;; db got text
	      (map
	       (lambda (item) (list 'td (format #f "~a" (cdr item)))) (cdr row))
	      ;; file link
	      (list `(td ,(make-link-for-files-list cd-id)
			 ;; adding cd id in same table delimeter
			 (input (@ (type "hidden") (id "id") (name "id")
				   (value ,(format #f "~a" (cdar row)))))))
	      ;; pic link
	      (list `(td ,(make-link-for-pics-list cd-id)))))))))


;; actual view definitions from here on
(cds-define show
	    ;; default page, just show all the db data and a query form to
	    ;; search in detail
	    ;; this could take query strings, but currently they're not parsed
	    (lambda (rc)
	      ;; rendering view from template
	      "<h1>This is cds#show</h1><p>Find me in app/views/cds/show.html.tpl</p>"

	      ;; database info
	      (define conn (connect-db 'mysql init-str))

	      (view-render "show" (the-environment))))


(cds-define show_pics
	    ;; page to show all pics for a specific cd
	    ;; this would be called as http://yourdomain/cds/show_pics?cd_id=XXX
	    ;; "<your-controller-name>-define <view>"
	    ;; means basically
	    ;; "get /<your-controller-name>/<view>"
	    ;; it's just a strange macro that defaults to GET requests for
	    ;; everything for some reason... not quite sure why this is useful
	    
	    (lambda (rc)
	      "<h1>This is cds#show_pics</h1><p>Find me in app/views/cds/show_pics.html.tpl</p>"
	      ;; give the cd id as argument/query string
	      ;; get all the files with *jpg *JPG *png *PNG in the dir and display them

	      (view-render "show_pics" (the-environment))))

(post "/cds/search" #:from-post 'qstr
      ;;is like (cds-define search ...) but for POST queries
      ;; the #:from-post 'qstr makes it possible to parse the POST body with
      ;; ":from-post". Basically telling artanis the body isn't a json string
      ;; this page searches for data according to the input form in /show (post)
      (lambda (rc)
	"<h1>This is cds#search</h1><p>Find me in app/views/cds/search.html.tpl</p>"

	(define conn (connect-db 'mysql init-str))

	(define make-sql-query-string-from-rc
	  ;; parse POST data and make mysql query string
	  (lambda ()
	    (let* (
		   (category (:from-post rc 'get "category"))
		   (cd-id (:from-post rc 'get "cd-id"))
		   (contents (:from-post rc 'get "contents"))
		   (artist (:from-post rc 'get "artist"))
		   (file (:from-post rc 'get "file")) ;; to grep this
		   ;; make sql string from above fields
		   (sql-conditions
		    (string-append
		     ;; actually I should make a macro or function for this code...
		     ;; maybe someday
		     (if (and category (not (string= category ""))) (string-append "category like '%" category "%' and ") "")
		     (if (and cd-id (not (string= cd-id ""))) (string-append "cd_id like '%" cd-id "%' and ") "")
		     (if (and artist (not (string= artist ""))) (string-append "artists like '%" artist "%' and ") "")
		     (if (and contents (not (string= contents ""))) (string-append "contents like '%" contents "%' and ") "")
		     )))
	      (define remove-last-part
		(lambda (some-string)
		  (xsubstring some-string 0 (- (string-length some-string) 4))))
	      ;; comments when trying this on a normal query string (not POST but GET)
	      ;; uri-decode apparently a super security risk, but no other way to insert spaces to mysql
	      ;; otherwise... no "htmlspecialchars_decode" or anything like that in artanis...
	      ;; (uri-decode some-string)))
	      ;; (uri-decode (xsubstring some-string 0 (- (string-length some-string) 4)))))

	      (string-append
	       "select * from description where "
	       ;; comments for when testing with GET requests
	       ;; this works for normal get requests with query strings
	       ;; (get-from-qstr rc "category")
	       ;; this works when #:from-post is not given
	       ;; (get-from-qstr  (utf8->string   (rc-body rc)) "category")
	       (remove-last-part sql-conditions)
	       " order by cd_id"))))

	(view-render "search" (the-environment))))

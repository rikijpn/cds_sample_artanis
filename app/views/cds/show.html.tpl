<!-- cds#show view template of cds
          Please add your license header here.
          This file is generated automatically by GNU Artanis. -->

<html>
  <head>
    <title><%= (current-appname) %>
    </title>
    <script>
       /*
        @licstart  The following is the entire license notice for the
        JavaScript code in this page.

        Copyright (C) 2014  Loic J. Duros

        The JavaScript code in this page is free software: you can
        redistribute it and/or modify it under the terms of the GNU
        General Public License (GNU GPL) as published by the Free Software
        Foundation, either version 3 of the License, or (at your option)
        any later version.  The code is distributed WITHOUT ANY WARRANTY;
        without even the implied warranty of MERCHANTABILITY or FITNESS
        FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

        As additional permission under GNU GPL version 3 section 7, you
        may distribute non-source (e.g., minimized or compacted) forms of
        that code without the copy of the GNU GPL normally required by
        section 4, provided you include this license notice and a URL
        through which recipients can access the Corresponding Source.


        @licend  The above is the entire license notice
        for the JavaScript code in this page.
        */
    </script>

  </head>
  <body>
    <h1>search</h1>
    <form action="search" method="post">
      category: <input type="text" name="category"><br />
      artist: <input type="text" name="artist"><br />
      contents: <input type="text" name="contents"><br />
      <input type="submit" value="Submit">
    </form>
    <h1>cds#show results</h1>
    <p>Rendered from app/views/cds/show.html.tpl.</p>
    <% (DB-query conn "set names utf8")
       (DB-query conn "select * from description order by cd_id")
       ;; necessary to show Japanese/not latin languages properly
       ;; and initial query just showing all
       %>
    <table>
     <tr>
       <th>ID</th>
       <th>Category</th>
       <th>Contents</th>
       <th>People/Artist</th>
       <th>File</th>
       <th>Pics</th>
     </tr>
    <%=
      (tpl->html
    (map print-row
;;    (cd-desc 'get 'description #:columns '(*) #:order-by "cd_id asc" ) ; this doesn't work, don't use the mapping functions, nothing but the simplest select works (order-by for example doesn't work...)
        (DB-get-all-rows conn) ; this works, simple enough
    ))
 %>
    </table>
  </body>
</html>

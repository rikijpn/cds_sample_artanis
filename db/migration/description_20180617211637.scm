;; Migration description_20180617211637 definition of test1
;; Please add your license header here.
;; This file is generated automatically by GNU Artanis.
(create-artanis-migration description_20180617211637) ; DO NOT REMOVE THIS LINE!!!

(migrate-create
 (create-table 'description))

  ;; (display "Add your create DB code\n"))
(migrate-up
 (create-table
  'description
  '(id auto (#:primary-key))
  '(cd_id char-field (#:maxlen 4))
  '(category char-field (#:maxlen 32))
  '(contents char-field (#:maxlen 1024))
  '(artists char-field (#:maxlen 256))
  ))

;; +----------+---------------+------+-----+---------+-------+
;; | Field    | Type          | Null | Key | Default | Extra |
;; +----------+---------------+------+-----+---------+-------+
;; | id       | varchar(4)    | YES  |     | NULL    |       |
;; | category | varchar(32)   | YES  |     | NULL    |       |
;; | contents | varchar(1024) | YES  |     | NULL    |       |
;; | artists  | varchar(256)  | YES  |     | NULL    |       |


(migrate-down
 (drop-table 'description))
